import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Artyom on 05.01.2016.
 */
public class ClassWriter {

    private String mOutPath;
    private String mName;
    private String mPackage;
    private FileWriter mWriter;

    private static final String H_PACKAGE = "package %s;\n\n";
    private static final String H_CLASS = "public class %s{\n\n";

    private static final String H_GETTER = "\tpublic %s(){\n\t\t%s\n\t}";

    public ClassWriter(String path, String name) {
        mName = name;
        mOutPath = path;
        if (Utils.isEmpty(mName)) {
            throw new AssertionError("Name is empty!");
        } else if (Utils.isEmpty(mOutPath)) {
            throw new AssertionError("Output path is empty!");
        }
    }

    public ClassWriter(String path, String name, String pack) {
        this(path, name);
        mPackage = pack;
    }

    public void create() throws IOException {
        if (mWriter == null) {
            File file = new File(mOutPath, mName);
            mWriter = new FileWriter(file);
            if (!Utils.isEmpty(mPackage)) {
                mWriter.write(String.format(H_PACKAGE, mPackage));
            }
            mWriter.write(String.format(H_CLASS, mName));
        } else throw new AssertionError("You should close writer before create new one!");
    }

    public void close() throws IOException {
        if (mWriter != null) {
            mWriter.write("}");
        } else throw new AssertionError("Writer not created!");
    }

    public void addGetter(String name, Object object) throws IOException {
        addGetter(name, Utils.typeOfObject(object));
    }

    public void addGetter(String name, String type) throws IOException {
        mWriter.write(String.format(H_GETTER, type, name));
    }


}
