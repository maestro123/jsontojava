/**
 * Created by Artyom on 05.01.2016.
 */
public class Utils {

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0 || str.trim().length() == 0;
    }

    public static String typeOfObject(Object object) {
        if (object instanceof Integer) {
            return "int";
        } else if (object instanceof Float) { //?????
            return "float";
        } else if (object instanceof Double) {
            return "double";
        } else if (object instanceof Long) {
            return "long";
        } else if (object instanceof String) {
            return "String";
        }
        throw new AssertionError("Unknown object type (" + object + ")");
    }

}
