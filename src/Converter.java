import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;

/**
 * Created by Artyom.Borovsky on 04.01.2016.
 */
public class Converter {

    public static final String TAG = Converter.class.getSimpleName();

    /*Required field*/
    private String mSource;
    private String mOutDir;

    /*Optional filed*/
    private String mPackageName;

    public Converter() {
    }

    public Converter setSource(String source) {
        mSource = source;
        return this;
    }

    public Converter setOutDir(String outDir) {
        mOutDir = outDir;
        return this;
    }

    public Converter setPackageName(String name) {
        mPackageName = name;
        return this;
    }

    public void convert() throws JSONException {
        JSONObject jsonObject = new JSONObject(mSource);
        HashMap<String, String> mClasses = new HashMap<String, String>();
        Stack<ClassWriter> mWriters = new Stack<ClassWriter>();
        Iterator<String> keys = jsonObject.keys();
        int index = 0;
        while (keys.hasNext()) {
            String key = keys.next();
            Object object = jsonObject.opt(key);
            ClassWriter mCurrentWriter = mWriters.peek();
            if (mCurrentWriter == null){
                mCurrentWriter = new ClassWriter(mOutDir, key, mPackageName);
                mWriters.addElement(mCurrentWriter);
            }
            if (object instanceof JSONArray) {

            }else if (object)


            if (mClasses.containsKey(key)) {

            }
            index++;
        }
    }


}
